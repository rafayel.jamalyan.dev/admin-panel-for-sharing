import type { Config } from "tailwindcss";

export const blackTitle = "#242424";

const config: Config = {
  content: ["./src/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic": "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "gradient-primary": "linear-gradient(300.07deg, #2F814D 17.87%, #82BE92 88.75%)",
        "gradient-gray": "linear-gradient(180deg, #4B4B4B 0%, #242424 100%)",
        "gradient-accent": "linear-gradient(89.27deg, #65C478 34.88%, #BCE8C1 108.24%)",
        "gradient-red": "linear-gradient(90deg, #D92B36 0%, #FFCBC3 109.95%)"
      },
      colors: {
        primary: {
          DEFAULT: "#2F814D",
          50: "#ECF6EE",
          100: "#C7E4CE",
          200: "#A3D1AF",
          300: "#82BE92",
          400: "#63AA78",
          500: "#479661",
          600: "#2F814D",
          700: "#1C6C3C",
          800: "#11572E",
          900: "#0D4223"
        },
        gray: {
          "text-holder": "#DBDBDB",
          50: "#F3F3F3",
          100: "#DDDDDD",
          200: "#C6C6C6",
          300: "#B0B0B0",
          400: "#9B9B9B",
          500: "#868686",
          600: "#727272",
          700: "#5E5E5E",
          800: "#4B4B4B"
        },
        black: {
          title: blackTitle
        },
        accent: {
          DEFAULT: "#65C478",
          50: "#E9F7EA",
          100: "#BCE8C1",
          200: "#91D69B",
          300: "#65C478",
          400: "#009C3F"
        },
        red: {
          DEFAULT: "#D92B36",
          50: "#FFEEEB",
          100: "#FFCBC3",
          200: "#FF877E",
          300: "#D92B36",
          400: "#9B021C"
        },
        orange: {
          DEFAULT: "#FFD5AD",
          50: "#FFF1E3",
          100: "#FFD5AD",
          200: "#F5942D",
          300: "#C67200"
        },
        purple: {
          DEFAULT: "#AD7FFF",
          50: "#F9F0FF",
          200: "#AD7FFF",
          300: "#5031B0",
          400: "#3E2587"
        }
      },
      fontSize: {
        h1: ["32px", { lineHeight: "1.2" }],
        h2: ["28px", { lineHeight: "1.2" }],
        h3: ["25px", { lineHeight: "30px" }],
        h4: ["22px", { lineHeight: "1.2" }],
        h5: ["20px", { lineHeight: "1.2" }],
        h6: ["18px", { lineHeight: "1.2" }],
        large: ["16px", { lineHeight: "1.2" }],
        base: ["14px", { lineHeight: "1.2" }],
        caption: ["13px", { lineHeight: "1.2" }],
        small: ["12px", { lineHeight: "1.2" }],
        xs: ["11px", { lineHeight: "1.2" }]
      },
      screens: {
        xs: "320px",
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px"
      },
      boxShadow: {
        primary100_2: "0px 0px 0px 2px #C7E4CE",
        primary100_4: "0px 0px 0px 4px #C7E4CE"
      }
    }
  },
  plugins: []
};
export default config;
