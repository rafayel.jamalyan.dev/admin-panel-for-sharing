import ApiSlice from "../slice";

export default class AuthSlice extends ApiSlice {
  static baseURL = ApiSlice.baseURL + "/auth";

  static GetSuperAdminToken(secret: string) {
    return this.request<{ token: string }>(`/super-admin-token?secret=${secret}`);
  }
}
