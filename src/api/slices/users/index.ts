import ApiSlice from "../../slice";
import { AlternatePaymentMethod, User, UserDetailed, UserSubscriptionFilter } from "./users-slice";

export default class UsersSlice extends ApiSlice {
  static baseURL = ApiSlice.baseURL + "/super-admin/users";
  static defaultAuth: boolean = true;

  static GetUsers(page: number, perPage: number, query = "", subscription?: UserSubscriptionFilter[]) {
    const params = new URLSearchParams({});
    params.append("page", page.toString());
    params.append("perPage", perPage.toString());
    if (query) params.append("query", query);
    if (subscription) for (const s of subscription) params.append("subscription[]", s);
    return this.request<{ items: User[]; total: number; usersCountWhoHasRequestedForPromocode: number }>(
      "?" + params.toString(),
      "GET"
    );
  }

  static GetUser(id: number) {
    return this.request<{ info: UserDetailed }>("/" + id, "GET");
  }

  static AddUser(user: FormData) {
    return this.request<User>("/", "POST", user);
  }

  static EditUser(id: number, user: FormData) {
    return this.request<User>("/" + id, "PUT", user);
  }

  static RemoveUser(id: number) {
    return this.request("/" + id, "DELETE");
  }

  static CreatePromocode(userId: number, paymentMethod?: AlternatePaymentMethod) {
    return this.request<{ promocode: string }>(`/${userId}/promocode`, "POST", { paymentMethod });
  }

  static UpgradeSubscription(
    userId: number,
    premium: boolean,
    startDate: string,
    endDate: string,
    alternatePaymentMethod?: AlternatePaymentMethod
  ) {
    return this.request(`/${userId}/subscription`, "PUT", {
      status: premium ? "on" : "off",
      startDate,
      endDate,
      alternatePaymentMethod
    });
  }
}
