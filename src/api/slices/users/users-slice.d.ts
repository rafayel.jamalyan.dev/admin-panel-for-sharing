export type UserSubscriptionFilter = "premium" | "free" | "pending";

export interface UserSubscription {
  startsAt: string;
  endsAt: string;
  status: "succeeded" | "pending" | "failed";
}

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  dateOfBirth: string | null;
  gender: "male" | "female";
  photo: string | null;
  isEmailVerified: boolean;
  subscription: UserSubscription | null;
  type: "customer" | "trainer";
  hasRequestedForPromocode: boolean;
  promocode: string | null;
}

export interface UserDetailed extends User {
  createdBySuperAdmin: boolean;
}

export enum AlternatePaymentMethod {
  Cash = "cash",
  Coupons = "coupons"
}
