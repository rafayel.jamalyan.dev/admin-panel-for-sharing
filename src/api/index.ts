import ApiSlice from "./slice";
import AuthSlice from "./slices/auth";
import UsersSlice from "./slices/users";

class Api extends ApiSlice {
  static auth = AuthSlice;
  static users = UsersSlice;
}

export default Api;
