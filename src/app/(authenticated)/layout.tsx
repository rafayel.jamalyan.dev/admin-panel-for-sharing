import RequireAuth from "@/features/auth/components/require-auth";
import DrawerMenu from "@/features/authenticated-layout/components/drawer-menu";
import Navbar from "@/features/authenticated-layout/components/navbar";
import { PropsWithChildren } from "react";

export default function AuthenticatedLayout({ children }: PropsWithChildren) {
  return (
    <RequireAuth>
      <div className="flex h-full w-full flex-nowrap">
        <div className="flex flex-shrink-0">
          <DrawerMenu />
        </div>
        <div className="flex flex-1 flex-shrink overflow-hidden bg-[#d9d9d9] bg-opacity-30">
          <div className="w-full overflow-x-hidden overflow-y-scroll p-8">
            <div className="flex flex-col gap-8">
              <div>
                <Navbar />
              </div>
              <div>{children}</div>
            </div>
          </div>
        </div>
      </div>
    </RequireAuth>
  );
}
