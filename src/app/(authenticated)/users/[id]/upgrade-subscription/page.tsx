import UpgradeSubscriptionOfUser from "@/features/users/upgrade-subscription/upgrade-subscription-of-user";

export default function UpgradeSubscriptionPage({ params }: { params: { id: string } }) {
  const userId = Number(params.id);

  if (Number.isNaN(userId)) {
    return <p>Invalid user id</p>;
  }

  return <UpgradeSubscriptionOfUser userId={userId} />;
}
