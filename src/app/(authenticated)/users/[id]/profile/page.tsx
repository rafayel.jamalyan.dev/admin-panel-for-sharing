import EditUserForm from "@/features/users/user-form/edit-user-form";

export default function ProfilePage({ params }: { params: { id: string } }) {
  const userId = Number(params.id);

  if (Number.isNaN(userId)) {
    return <p>Invalid user id</p>;
  }

  return <EditUserForm userId={userId} />;
}
