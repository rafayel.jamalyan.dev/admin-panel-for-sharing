import AddUserForm from "@/features/users/user-form/add-user-form";

export default function CreateNewUserPage() {
  return (
    <div className="w-[704px] rounded-[24px] bg-white">
      <AddUserForm />
    </div>
  );
}
