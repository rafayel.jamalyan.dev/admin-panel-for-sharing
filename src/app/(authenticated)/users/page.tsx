import UsersTableWrapper from "@/features/users/users-table/users-table-wrapper";

export default function UsersPage() {
  return (
    <div className="w-full">
      <div className="rounded-[24px] bg-white">
        <UsersTableWrapper />
      </div>
    </div>
  );
}
