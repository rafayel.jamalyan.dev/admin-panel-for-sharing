import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import ClientOnlySnackbarProvider from "@/common/components/snackbar-provider";
import GlobalLoadingProvider from "@/common/components/global-loading-provider";

const inter = Inter({ subsets: ["latin"], weight: ["400", "500", "700"] });

export const metadata: Metadata = {
  title: "Leanzer | Admin panel",
  description: "Leanzer | Admin panel"
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className={`flex h-[100dvh] w-screen ${inter.className} overflow-hidden`}>
        {children}
        <ClientOnlySnackbarProvider />
        <GlobalLoadingProvider />
        <div id="modal-root" />
      </body>
    </html>
  );
}
