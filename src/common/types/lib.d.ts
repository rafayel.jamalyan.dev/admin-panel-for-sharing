import type { ResponseType } from "axios";

export type RequestOptions = {
  auth?: boolean;
  headers?: Record<string, unknown>;
  responseType?: ResponseType;
};

export type ResponseModel<T = unknown> = {
  meta: {
    error: null | {
      code: number;
      message: string;
      info?: unknown;
    };
    status: number;
  };
  data: T;
};
