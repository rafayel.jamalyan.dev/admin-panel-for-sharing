"use client";

import React, { InputHTMLAttributes, useRef, useState } from "react";

export interface InputProps extends Omit<InputHTMLAttributes<HTMLInputElement>, "children" | "placeholder"> {
  label?: string;
  placeholder?: string | React.ReactNode;
  endIcons?: React.ReactNode;
  containerClassName?: string;
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(function Input(
  { label, placeholder, endIcons, containerClassName, className, onChange, ...inputProps },
  ref
) {
  const localRef = useRef<HTMLInputElement | null>(null);

  const [showCustomPlaceholder, setShowCustomPlaceholder] = useState(
    Boolean(typeof placeholder !== "string" && !inputProps.value && !inputProps.defaultValue)
  );
  const { disabled, readOnly } = inputProps;

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    setShowCustomPlaceholder(Boolean(typeof placeholder !== "string" && !e.target.value));
    onChange?.(e);
  }

  return (
    <div className={`relative flex w-full flex-col ${containerClassName}`}>
      {label && (
        <label
          className={`mb-[6px] text-large font-medium ${disabled ? "text-gray-300" : "text-black-title"}`}
          htmlFor={inputProps.id}
        >
          {label}
        </label>
      )}
      <div className="relative">
        <input
          ref={el => {
            localRef.current = el;
            if (typeof ref === "function") ref(el);
            else if (ref) ref.current = el;
          }}
          placeholder={typeof placeholder === "string" ? placeholder : undefined}
          className={`
            text-title-color h-[41px] w-full
            rounded-[4px] px-3 py-2 text-base font-normal text-black-title
            outline-none
            placeholder:text-gray-300
            ${disabled ? "border-gray-300 bg-white text-gray-300" : "hover:border-gray-600"}
            ${readOnly ? "bg-gray-50" : "border border-gray-text-holder focus:border-primary focus:shadow-primary100_2"}
            ${className}
          `}
          type="text"
          onChange={e => handleChange(e)}
          {...inputProps}
        />
        {showCustomPlaceholder && (
          <div className="absolute left-3 top-1/2 -translate-y-1/2" onClick={() => localRef.current?.focus()}>
            {placeholder}
          </div>
        )}
        {endIcons && <div className="absolute right-3 top-0 h-full">{endIcons}</div>}
      </div>
    </div>
  );
});

export default Input;
