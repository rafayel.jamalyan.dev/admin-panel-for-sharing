"use client";

import React, { SelectHTMLAttributes } from "react";
import Icon from "./icon";

export interface SelectProps extends SelectHTMLAttributes<HTMLSelectElement> {
  label?: string;
  containerClassName?: string;
  readOnly?: boolean;
}

const Select = React.forwardRef<HTMLSelectElement, SelectProps>(function Select(
  { label, containerClassName, className, children, readOnly, ...selectProps },
  ref
) {
  const { disabled } = selectProps;

  return (
    <div className={`relative flex w-full flex-col ${containerClassName}`}>
      {label && (
        <label
          className={`mb-[6px] text-large font-medium ${disabled ? "text-gray-300" : "text-black-title"}`}
          htmlFor={selectProps.id}
        >
          {label}
        </label>
      )}
      <div className="relative">
        <select
          ref={ref}
          className={`
            text-title-color h-[41px] w-full select-none appearance-none
            rounded-[4px] border border-gray-text-holder px-3 py-2 text-base
            font-normal text-black-title
            outline-none placeholder:text-gray-300 focus:border-primary focus:shadow-primary100_2
            ${disabled ? "border-gray-300 bg-white text-gray-300" : "hover:border-gray-600"}
            ${readOnly ? "border-0 bg-gray-50" : ""}
            ${className}
          `}
          {...selectProps}
          disabled={readOnly || selectProps.disabled}
        >
          {children}
        </select>
        <div className="absolute right-3 top-0 flex h-full items-center justify-center">
          <Icon name="arrow-down-gray" size={16} />
        </div>
      </div>
    </div>
  );
});

export default Select;
