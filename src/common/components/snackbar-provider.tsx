"use client";

import { SnackbarProvider } from "notistack";
import { withClientOnly } from "./client-only";

function ClientOnlySnackbarProvider() {
  return <SnackbarProvider />;
}

export default withClientOnly(ClientOnlySnackbarProvider);
