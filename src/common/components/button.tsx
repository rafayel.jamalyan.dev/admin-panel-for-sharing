"use client";

import React, { ButtonHTMLAttributes, PropsWithChildren, useContext, useState } from "react";

export type ButtonVariant = "filled" | "outlined" | "ghost";
export type ButtonColor = "primary" | "black" | "white";

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  variant?: ButtonVariant;
  color?: ButtonColor;
  startIcon?: React.ReactNode;
  endIcon?: React.ReactNode;
}

function Button({
  onClick,
  variant = "filled",
  color = "primary",
  children,
  className,
  startIcon,
  endIcon,
  ...buttonAttributes
}: PropsWithChildren<Props>) {
  const [hover, setHover] = useState(false);
  const [pressed, setPressed] = useState(false);

  const { disabled } = buttonAttributes;

  return (
    <ButtonContext.Provider value={{ color, hover, pressed, variant }}>
      <button
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        onMouseDown={() => setPressed(true)}
        onMouseUp={() => setPressed(false)}
        onTouchStart={() => setPressed(true)}
        onTouchEnd={() => setPressed(false)}
        className={`
          box-border flex items-center justify-center gap-1 rounded-lg px-4 py-3 text-base font-medium
          ${
            variant === "filled"
              ? `${
                  color === "primary"
                    ? `bg-primary text-white ${
                        !disabled ? "hover:bg-primary-700 active:bg-primary active:shadow-primary100_4" : ""
                      }`
                    : ""
                }`
              : variant === "outlined"
              ? `${
                  color === "primary"
                    ? `border border-primary px-[15px] py-[11px] text-primary ${
                        !disabled
                          ? "hover:border-primary-700 hover:text-primary-700 active:border-primary active:text-primary active:shadow-primary100_4"
                          : ""
                      }`
                    : ""
                }`
              : variant === "ghost"
              ? `${
                  color === "primary"
                    ? `text-primary ${
                        !disabled
                          ? "hover:bg-primary-50 hover:text-primary-700 active:text-primary active:shadow-primary100_4"
                          : ""
                      }`
                    : ""
                }`
              : ""
          }

          ${
            disabled
              ? variant === "filled"
                ? "!bg-gray-300 opacity-40"
                : variant === "outlined"
                ? "!border-gray-300 opacity-40"
                : ""
              : ""
          }
          ${className}
        `}
        onClick={onClick}
        {...buttonAttributes}
      >
        {startIcon}
        {children}
        {endIcon}
      </button>
    </ButtonContext.Provider>
  );
}

interface ButtonContext {
  color?: ButtonColor;
  variant?: ButtonVariant;
  hover?: boolean;
  pressed?: boolean;
}

export const ButtonContext = React.createContext<ButtonContext | undefined>(undefined);

export function useButtonContext() {
  return useContext(ButtonContext);
}

export default Button;
