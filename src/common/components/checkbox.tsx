import Image from "next/image";
import React, { InputHTMLAttributes, useRef, useState } from "react";

export interface CheckboxProps extends Omit<InputHTMLAttributes<HTMLInputElement>, "children"> {
  containerClassName?: string;
  size?: number;
  label?: string;
  labelClassName?: string;
}

const Checkbox = React.forwardRef<HTMLInputElement, CheckboxProps>(function Checkbox(
  { containerClassName, size = 16, label, labelClassName, ...checkboxProps },
  forwardedRef
) {
  const [checked, setChecked] = useState(checkboxProps.checked || false);
  const ref = useRef<HTMLInputElement>(null);

  function handleChange() {
    setChecked(!checked);
    if (ref.current) {
      ref.current.click();
    }
  }

  return (
    <div className={`relative ${containerClassName}`}>
      <div className="flex flex-nowrap gap-2">
        <div className="cursor-pointer">
          <Image
            alt="checkbox"
            src={checked ? "/images/checkbox_checked.svg" : "/images/checkbox_unchecked.png"}
            width="0"
            height="0"
            style={{
              width: size,
              height: size
            }}
            onClick={handleChange}
            unoptimized
          />
        </div>
        {label && (
          <label className={`cursor-pointer text-base font-medium text-black-title ${labelClassName}`} onClick={handleChange}>
            {label}
          </label>
        )}
      </div>
      <input
        type="checkbox"
        className="hidden"
        ref={(elm: HTMLInputElement) => {
          if (typeof forwardedRef === "function") {
            forwardedRef(elm);
          } else if (forwardedRef) {
            forwardedRef.current = elm;
          }
          // make ref.current not readonly
          (ref as any).current = elm;
        }}
        {...checkboxProps}
      />
    </div>
  );
});

export default Checkbox;
