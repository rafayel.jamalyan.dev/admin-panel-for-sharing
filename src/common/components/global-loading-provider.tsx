"use client";

import { useEffect, useState } from "react";
import { withClientOnly } from "./client-only";

const loadingObject = {
  startGlobalLoading: () => {},
  stopGlobalLoading: () => {}
};

function GlobalLoadingProvider() {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    loadingObject.startGlobalLoading = () => setLoading(true);
    loadingObject.stopGlobalLoading = () => setLoading(false);
  }, []);

  if (!loading) return null;

  return (
    <div className="fixed left-0 top-0 flex h-screen w-screen items-center justify-center bg-black-title bg-opacity-20">
      <div className="h-32 w-32 animate-spin rounded-full border-b-2 border-t-2 border-gray-900"></div>
    </div>
  );
}

export function startGlobalLoading() {
  loadingObject.startGlobalLoading();
}

export function stopGlobalLoading() {
  loadingObject.stopGlobalLoading();
}

export default withClientOnly(GlobalLoadingProvider);
