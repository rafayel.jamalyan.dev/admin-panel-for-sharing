"use client";

import React, { ButtonHTMLAttributes } from "react";

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  onClick?: () => void;
  selected?: boolean;
}

function SelectionButton({ onClick, selected, className, children, ...buttonAttributes }: Props) {
  const { disabled } = buttonAttributes;
  return (
    <button
      className={`
        box-border rounded-lg border border-gray-50 px-2.5 py-3 text-large font-medium text-black-title
        ${!disabled ? "hover:border-gray-600 active:border-gray-50 active:shadow-primary100_4" : ""}
        ${selected ? "!border-2 !border-primary hover:!border-primary-700" : ""}
        ${disabled ? "!text-gray-200" : ""}
        ${className}
      `}
      onClick={onClick}
      {...buttonAttributes}
    >
      {children}
    </button>
  );
}

export default SelectionButton;
