export function getFile(src: string) {
  const fileHost = process.env.NEXT_PUBLIC_FILE_HOST || process.env.NEXT_PUBLIC_API_HOST;
  return src.startsWith("http://") || src.startsWith("https://") ? src : `${fileHost}/files/${src}`;
}

export function objectToFormData(obj: object) {
  const formData = new FormData();
  Object.keys(obj).forEach(key => {
    const value = (obj as any)[key];
    if (value instanceof Array) {
      value.forEach((v: any) => {
        formData.append(key, v);
      });
    } else {
      formData.append(key, value);
    }
  });
  return formData;
}
