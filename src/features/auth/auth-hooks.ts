import { useCallback } from "react";
import useAuthStore from "./auth-store";
import Api from "@/api";
import { ADMIN_ID } from "./data";
import ApiSlice from "@/api/slice";

export function useAdminAuth() {
  const { isLoggedIn, setToken, setInfo, token } = useAuthStore(state => ({
    isLoggedIn: Boolean(state.token && state.info),
    setToken: state.setToken,
    setInfo: state.setInfo,
    token: state.token
  }));

  const loginAsAdmin = useCallback(async () => {
    const secret = getSuperAdminSecret();
    const rsp = await Api.auth.GetSuperAdminToken(secret);
    if (rsp.meta.error) {
      alert("Wrong secret, try again");
      await loginAsAdmin();
      return;
    }
    setToken(rsp.data.token);
    setInfo({ id: ADMIN_ID });
    ApiSlice.setToken(rsp.data.token);
  }, [setToken, setInfo]);

  return { isLoggedIn, loginAsAdmin, token };
}

function getSuperAdminSecret(): string {
  const secret = prompt("Enter super admin secret");
  if (secret) {
    return secret;
  }
  alert("You must enter a super admin secret");
  return getSuperAdminSecret();
}
