"use client";

import { useRouter } from "next/navigation";
import React, { PropsWithChildren, useEffect, useState } from "react";
import { useAdminAuth } from "../auth-hooks";
import { withClientOnly } from "@/common/components/client-only";
import ApiSlice from "@/api/slice";

function RequireAuth({ children, navigateToUsersPage }: PropsWithChildren<{ navigateToUsersPage?: boolean }>) {
  const router = useRouter();
  const navigate = router.push;
  const [tokenIsSetForApi, setTokenIsSetForApi] = useState(false);

  const { isLoggedIn, loginAsAdmin, token } = useAdminAuth();

  // go to /users if logged in
  // else login
  useEffect(() => {
    if (isLoggedIn) {
      if (navigateToUsersPage) navigate("/users");
    } else {
      loginAsAdmin();
    }
  }, [isLoggedIn, navigate, loginAsAdmin, navigateToUsersPage]);

  // check the token in api slice
  useEffect(() => {
    if (token) {
      if (!ApiSlice.token) {
        ApiSlice.setToken(token);
      }
      setTokenIsSetForApi(true);
    }
  }, [token]);

  return isLoggedIn && tokenIsSetForApi ? <>{children}</> : <></>;
}

export default withClientOnly(RequireAuth);
