import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";

interface Admin {
  id: number;
}

interface AuthStoreState {
  token: string | null;
  info: Admin | null;
}

interface AuthStoreActions {
  setToken(_token: string | null): void;
  setInfo(_info: Admin | null): void;
}

const useAuthStore = create<AuthStoreState & AuthStoreActions>()(
  persist(
    set => ({
      token: null,
      setToken(token) {
        set(() => ({ token }));
      },
      info: null,
      setInfo(info) {
        set(() => ({ info }));
      }
    }),
    {
      name: "auth-storage",
      storage: createJSONStorage(() => localStorage)
    }
  )
);

export default useAuthStore;
