import IconButton from "@/common/components/icon-button";
import Select from "@/common/components/select";

interface TablePaginationProps {
  page: number;
  setPage: (page: number) => void;
  perPage: number;
  setPerPage: (perPage: number) => void;
  total: number;
  pageCount: number;
}

export default function TablePagination({ page, setPage, perPage, setPerPage, total, pageCount }: TablePaginationProps) {
  return (
    <div className="flex items-center justify-between">
      <p className="text-small font-medium text-gray-500">
        {(page - 1) * perPage + 1}-{Math.min(page * perPage, total)} of {total}
      </p>
      <div className="flex items-center gap-8">
        <div>
          <div className="flex items-center gap-0.5">
            <p className="w-max flex-shrink-0 text-small font-medium text-gray-600">Rows per page:</p>
            <Select
              className="w-[44px] !border-none !px-0 text-small font-medium text-gray-600 !shadow-none"
              value={perPage}
              onChange={e => setPerPage(Number(e.target.value))}
            >
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="50">50</option>
            </Select>
          </div>
        </div>
        <div>
          <div className="flex items-center justify-center gap-2">
            <IconButton name="arrow-left" color="black" size={16} onClick={() => setPage(Math.max(1, page - 1))} />
            <p className="font-small font-medium text-gray-600">
              <span className="text-black-title">{page}</span>/{pageCount}
            </p>
            <IconButton name="arrow-right" color="black" size={16} onClick={() => setPage(Math.min(pageCount, page + 1))} />
          </div>
        </div>
      </div>
    </div>
  );
}
