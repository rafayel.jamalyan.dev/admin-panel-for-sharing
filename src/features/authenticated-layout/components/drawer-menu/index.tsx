"use client";

import Image from "next/image";
import React, { useEffect } from "react";
import { drawerMenuPages } from "./drawer-menu-data";
import DrawerMenuItem from "../drawer-menu-item";
import { usePathname } from "next/navigation";
import useDrawerStore from "../../drawer-store";

function DrawerMenu() {
  const { open, setActivePagePathname } = useDrawerStore(store => ({
    open: store.isDrawerOpen,
    setActivePagePathname: store.setActivePagePathname
  }));
  const pathname = usePathname();

  // Set active page on pathname change
  useEffect(() => {
    const pathnameWithoutSlash = pathname.slice(1);
    if (pathnameWithoutSlash.includes("users/")) {
      if (pathnameWithoutSlash.includes("/upgrade-subscription")) return setActivePagePathname("users/[id]/upgrade-subscription");
      else if (pathnameWithoutSlash.includes("/profile")) return setActivePagePathname("users/[id]/profile");
    }
    setActivePagePathname(pathnameWithoutSlash);
  }, [pathname, setActivePagePathname]);

  return (
    <div className="flex-1 bg-red-200 bg-gradient-gray px-8 py-10">
      <div className="flex flex-col items-start gap-8">
        <div>
          <Image src="/images/logo-large.svg" width="0" height="0" alt="logo" style={{ width: 80, height: 26 }} />
        </div>
        <div className={`${open ? "w-[247px]" : "w-[72px]"} transition-[width] duration-300 ease-out`}>
          {drawerMenuPages.map(page => (
            <DrawerMenuItem key={page.path} info={page} open={open} active={page.path === pathname} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default DrawerMenu;
