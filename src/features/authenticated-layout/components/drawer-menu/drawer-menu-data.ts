export interface DrawerMenuPage {
  title: string;
  icon: string;
  path: string;
}

export const drawerMenuPages: DrawerMenuPage[] = [
  {
    title: "Users",
    icon: "page-users",
    path: "/users"
  },
  {
    title: "Notifications",
    icon: "page-notifications",
    path: "/notifications"
  },
  {
    title: "Activity",
    icon: "page-activity",
    path: "/activity"
  },
  {
    title: "Nutrition",
    icon: "page-nutrition",
    path: "/nutrition"
  }
];
