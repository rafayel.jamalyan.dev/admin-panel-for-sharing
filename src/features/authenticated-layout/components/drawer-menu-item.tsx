import React, { useState } from "react";
import { DrawerMenuPage } from "./drawer-menu/drawer-menu-data";
import Icon from "@/common/components/icon";
import Link from "next/link";
import { resetUsersTableCache } from "@/features/users/users-table/hooks";

interface Props {
  info: DrawerMenuPage;
  open: boolean;
  active: boolean;
}

function DrawerMenuItem({ info, open, active }: Props) {
  const [pressed, setPressed] = useState(false);
  const [hover, setHover] = useState(false);

  function handleClick() {
    if (info.path === "/users") {
      resetUsersTableCache();
    }
  }

  return (
    <div
      className={`
        h-[56px] w-full overflow-hidden rounded-lg  ${active || pressed ? "border-2" : ""}
        ${active ? "border-gray-700" : ""} ${pressed ? "!border-accent-400" : ""} ${hover && !pressed ? "bg-gray-700" : ""}
      `}
    >
      <Link
        href={info.path}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => {
          setHover(false);
          setPressed(false);
        }}
        onMouseDown={() => setPressed(true)}
        onMouseUp={() => setPressed(false)}
        onTouchStart={() => setPressed(true)}
        onTouchEnd={() => setPressed(false)}
        className="h-full"
        onClick={handleClick}
      >
        <div
          className={`
            flex h-full w-max flex-nowrap  items-center
            gap-4 ${active || pressed ? "px-[22px] py-[14px]" : "px-6 py-4"}
          `}
        >
          <div className="h-6 w-6">
            <Icon name={info.icon} size={24} />
          </div>
          <div className={`flex flex-col  ${open ? "opacity-1" : "opacity-0"}  transition-[opacity] duration-300 ease-out`}>
            <div className="text-base font-normal text-white">{info.title}</div>
          </div>
        </div>
      </Link>
    </div>
  );
}

export default DrawerMenuItem;
