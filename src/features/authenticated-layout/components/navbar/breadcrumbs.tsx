import Link from "next/link";
import { Breadcrumb } from "./navbar-data";
import React from "react";

interface Props {
  items: Breadcrumb[];
}

export default function Breadcrumbs({ items }: Props) {
  return (
    <div className="flex flex-nowrap items-center">
      {items.map((item, i) => (
        <p className="text-base font-normal text-gray-800" key={`breadcrumb-${i}-${item.title}`}>
          {i !== 0 && <span>/</span>}

          {item.path ? (
            <Link href={item.path} key={item.path}>
              <span className="px-4 py-1">{item.title}</span>
            </Link>
          ) : (
            <span className="px-4 py-1">{item.title}</span>
          )}
        </p>
      ))}
    </div>
  );
}
