import Api from "@/api";
import { AlternatePaymentMethod } from "@/api/slices/users/users-slice.d";
import Button from "@/common/components/button";
import { startGlobalLoading, stopGlobalLoading } from "@/common/components/global-loading-provider";
import Icon from "@/common/components/icon";
import Modal from "@/common/components/modal";
import { enqueueSnackbar } from "notistack";
import React, { useCallback, useState } from "react";
import { Props as ModalProps } from "react-modal";
import { UserPaymentMethodPickerModalContent } from "./user-payment-method-picker-modal";
import { useUsersContext } from "./users-table/hooks";

interface Props {
  userId: number | null;
}

function GenerateCodeModal({ userId, ...modalProps }: ModalProps & Props) {
  const { generateCode, generatedCode } = useGenerateCode();

  function copy() {
    if (!generatedCode) return;

    navigator.clipboard.writeText(generatedCode);
    enqueueSnackbar("Code copied to clipboard", { variant: "success", autoHideDuration: 2000 });
  }

  const { setUsers, setUsersCountWhoHasRequestedForPromocode } = useUsersContext();

  async function handleGenerateCode(method: AlternatePaymentMethod) {
    if (!userId) return;
    const newCode = await generateCode(userId, method);
    if (newCode) {
      setUsers(users => {
        if (users === null) return null;
        return users.map(user => {
          if (user.id === userId) {
            if (user.hasRequestedForPromocode) {
              setUsersCountWhoHasRequestedForPromocode(count => count - 1);
            }
            return { ...user, promocode: newCode, hasRequestedForPromocode: true };
          } else return user;
        });
      });
    }
  }

  return (
    <Modal {...modalProps}>
      {generatedCode === null ? (
        <UserPaymentMethodPickerModalContent onSelect={method => handleGenerateCode(method)} />
      ) : (
        <div className="rounded-[24px] bg-white">
          <div className="px-[120px] py-[48px]">
            <div className="flex flex-col items-center gap-6">
              <div className="flex justify-center">
                <Icon name="code-black" size={48} />
              </div>
              <div className="flex flex-col items-center gap-2">
                <p className="text-h1 font-bold text-black-title">{generatedCode}</p>
                <p className="text-h2 font-normal text-black-title">Generated Code Number</p>
              </div>
            </div>
          </div>
          <div className="border-t border-gray-300 p-[10px]">
            <div className="flex justify-center">
              <Button variant="ghost" startIcon={<Icon name="copy" />} onClick={copy}>
                Copy Code
              </Button>
            </div>
          </div>
        </div>
      )}
    </Modal>
  );
}

const codeCache = new Map<number, string>();

export function useGenerateCode() {
  const [generatedCode, setGeneratedCode] = useState<string | null>(null);
  const reset = useCallback(() => setGeneratedCode(null), []);

  const generateCode = useCallback(async (userId: number, AlternatePaymentMethod?: AlternatePaymentMethod) => {
    if (codeCache.has(userId)) {
      setGeneratedCode(codeCache.get(userId)!);
      return;
    }

    startGlobalLoading();
    const rsp = await Api.users.CreatePromocode(userId, AlternatePaymentMethod);
    stopGlobalLoading();
    if (rsp.meta.error) {
      enqueueSnackbar(rsp.meta.error.message, { variant: "error", autoHideDuration: 2000 });
      return;
    }

    const newCode = rsp.data.promocode;
    codeCache.set(userId, newCode);
    setGeneratedCode(newCode);
    return newCode;
  }, []);

  return { generateCode, generatedCode, resetGeneratedCode: reset };
}

export default GenerateCodeModal;
