"use client";

import Api from "@/api";
import UserPhoto from "../user-photo";
import UserSubscriptionChip from "../user-subscription-chip";
import { getUserSubscriptionStatus } from "../utils";
import Toggle from "@/common/components/toggle";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import UpgradeSubscriptionForm from "./upgrade-subscription-form";

interface Props {
  userId: number;
}

export default function UpgradeSubscriptionOfUser({ userId }: Props) {
  const { data: userRsp, success } = Api.useApi(() => Api.users.GetUser(userId), [userId]);
  const user = (success && userRsp?.info) || null;
  const [premium, setPremium] = useState<boolean>();
  const router = useRouter();

  // initialize premium state
  useEffect(() => {
    if (!user) return;
    const isPremium = getUserSubscriptionStatus(user) === "premium";
    setPremium(isPremium);
  }, [user]);

  if (!user) {
    return null;
  }

  return (
    <div className="flex w-[704px] flex-col gap-8">
      <div className="flex justify-between">
        <div className="border-l-[5px] border-primary pl-[27px]">
          <div className="flex gap-2">
            <div className="relative aspect-square w-[66px] overflow-hidden rounded-full">
              <UserPhoto photo={user.photo} />
            </div>
            <div>
              <p className="text-large font-medium text-black-title">{user.name}</p>
              <p className="mt-1 text-base font-normal text-gray-800">{user.email}</p>
              <div>
                <UserSubscriptionChip status={getUserSubscriptionStatus(user)} />
              </div>
            </div>
          </div>
        </div>
        <div>
          <div className="flex items-center gap-2">
            {premium ? (
              <p className="text-small text-black-title">
                Switch to <br /> Standard
              </p>
            ) : (
              <p className="text-small text-black-title">
                Upgrade to <br /> Premium
              </p>
            )}
            <Toggle offMode="dark-gray" checked={premium} onChange={e => setPremium(e.target.checked)} />
          </div>
        </div>
      </div>
      <div className="rounded-[32px] bg-white">
        {premium !== undefined && <UpgradeSubscriptionForm premium={premium} onSubmit={() => router.back()} user={user} />}
      </div>
    </div>
  );
}
