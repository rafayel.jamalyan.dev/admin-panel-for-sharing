import React, { useRef } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import { Controller, useForm } from "react-hook-form";
import * as z from "zod";
import { enqueueSnackbar } from "notistack";
import { startGlobalLoading, stopGlobalLoading } from "@/common/components/global-loading-provider";
import Api from "@/api";
import { AlternatePaymentMethod, User } from "@/api/slices/users/users-slice.d";
import Input from "@/common/components/input";
import Button from "@/common/components/button";
import { format } from "date-fns";
import UserPaymentMethodPickerModal from "../user-payment-method-picker-modal";
import { useBooleanValue } from "@/common/hooks/values";

const validationSchema = z.object({
  startDate: z.string().regex(/^\d{4}-\d{2}-\d{2}$/),
  endDate: z.string().regex(/^\d{4}-\d{2}-\d{2}$/)
});

type ValidationSchema = z.infer<typeof validationSchema>;

interface Props {
  premium: boolean;
  onSubmit: () => void;
  user: User;
}

function UpgradeSubscriptionForm({ premium, onSubmit, user }: Props) {
  const {
    value: isPaymentMethodPickerModalOpen,
    makeTrue: openPaymentMethodPickerModal,
    makeFalse: closePaymentMethodPickerModal
  } = useBooleanValue();

  const {
    control,
    handleSubmit,
    formState: { isDirty },
    getValues
  } = useForm({
    resolver: zodResolver(validationSchema),
    defaultValues: {
      startDate: user.subscription?.startsAt ? format(new Date(user.subscription.startsAt), "yyyy-MM-dd") : "",
      endDate: user.subscription?.endsAt ? format(new Date(user.subscription.endsAt), "yyyy-MM-dd") : ""
    }
  });

  const initialPremiumStatus = useRef(premium);
  const isPremiumChanged = initialPremiumStatus.current !== premium;

  async function upgradeSubscription(startDate: string, endDate: string, alternatePaymentMethod?: AlternatePaymentMethod) {
    startGlobalLoading();
    const rsp = await Api.users.UpgradeSubscription(user.id, premium, startDate, endDate, alternatePaymentMethod);
    stopGlobalLoading();
    if (rsp.meta.error) {
      return enqueueSnackbar("Failed to upgrade subscription", { variant: "error", autoHideDuration: 2000 });
    }

    enqueueSnackbar("Subscription upgraded", { variant: "success", autoHideDuration: 2000 });
    onSubmit();
  }

  async function submit(data: ValidationSchema) {
    if (data.endDate <= data.startDate) {
      return enqueueSnackbar("End date must be greater than start date", { variant: "error", autoHideDuration: 2000 });
    }

    if (premium && data.endDate < format(new Date(), "yyyy-MM-dd")) {
      return enqueueSnackbar("Start date must be greater than today", { variant: "error", autoHideDuration: 2000 });
    }

    if (isPremiumChanged && premium) {
      // making user premium
      openPaymentMethodPickerModal();
      return;
    }

    await upgradeSubscription(data.startDate, data.endDate);
  }

  return (
    <form onSubmit={handleSubmit(submit)}>
      <div className="p-8">
        <div className="flex flex-col gap-12">
          <div>
            <p className="text-h6 font-medium text-black-title">Set Premium Dates</p>
            <p className="mt-2 text-base font-normal text-gray-800">Set the start and end dates for user premium access.</p>
          </div>
          <div className="grid grid-cols-2 gap-4">
            <div>
              <Controller
                name="startDate"
                control={control}
                render={({ field }) => <Input {...field} readOnly={!premium} label="Start Date" type="date" />}
              />
            </div>

            <div>
              <Controller
                name="endDate"
                control={control}
                render={({ field }) => <Input {...field} readOnly={!premium} label="End Date" type="date" />}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="border-t border-gray-100 px-8 py-4">
        <div className="flex justify-end">
          <Button disabled={!(isPremiumChanged || isDirty)} type="submit">
            Save Changes
          </Button>
        </div>
      </div>
      <UserPaymentMethodPickerModal
        isOpen={isPaymentMethodPickerModalOpen}
        onRequestClose={closePaymentMethodPickerModal}
        onSelect={method => {
          closePaymentMethodPickerModal();
          upgradeSubscription(getValues().startDate, getValues().endDate, method);
        }}
      />
    </form>
  );
}

export default UpgradeSubscriptionForm;
