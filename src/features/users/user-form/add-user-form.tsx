"use client";

import React from "react";
import UserForm, { UserFormData } from "./user-form";
import { startGlobalLoading, stopGlobalLoading } from "@/common/components/global-loading-provider";
import { enqueueSnackbar } from "notistack";
import { _USER_ALREADY_EXISTS_ } from "@/api/error-codes";
import Api from "@/api";
import { objectToFormData } from "@/common/lib";
import { useRouter } from "next/navigation";
import Button from "@/common/components/button";
import Icon from "@/common/components/icon";

function AddUserForm() {
  const router = useRouter();

  async function handleSubmit(payload: UserFormData) {
    const formData = objectToFormData(payload);
    startGlobalLoading();
    const rsp = await Api.users.AddUser(formData);
    stopGlobalLoading();
    if (rsp.meta.error) {
      let errorMessage = "Failed to add user!";
      if (rsp.meta.error.code === _USER_ALREADY_EXISTS_) {
        errorMessage = "User with specified email or username already exists!";
      }
      return enqueueSnackbar(errorMessage, { variant: "error" });
    }
    enqueueSnackbar("User added successfully", { variant: "success" });
    router.push("/users");
  }

  return (
    <UserForm
      onSubmit={handleSubmit}
      submitBtn={
        <Button startIcon={<Icon name="plus" size={16} />} type="submit">
          Add User
        </Button>
      }
    />
  );
}

export default AddUserForm;
