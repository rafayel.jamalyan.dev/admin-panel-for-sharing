"use client";

import Api from "@/api";
import React from "react";
import UserForm, { UserFormData } from "./user-form";
import Button from "@/common/components/button";
import { objectToFormData } from "@/common/lib";
import { startGlobalLoading, stopGlobalLoading } from "@/common/components/global-loading-provider";
import { _USER_ALREADY_EXISTS_ } from "@/api/error-codes";
import { enqueueSnackbar } from "notistack";

interface Props {
  userId: number;
}

function EditUserForm({ userId }: Props) {
  const { data, success } = Api.useApi(() => Api.users.GetUser(userId));
  const user = (success && data?.info) || null;

  if (!user) return <></>;

  async function handleSubmit(payload: UserFormData) {
    const formData = objectToFormData(payload);
    startGlobalLoading();
    const rsp = await Api.users.EditUser(userId, formData);
    stopGlobalLoading();
    if (rsp.meta.error) {
      let errorMessage = "Failed to edit user!";
      if (rsp.meta.error.code === _USER_ALREADY_EXISTS_) {
        errorMessage = "User with specified email or username already exists!";
      }
      return enqueueSnackbar(errorMessage, { variant: "error" });
    }
    enqueueSnackbar("User edited successfully", { variant: "success" });
  }

  return (
    <div className="w-[704px] rounded-[24px] bg-white">
      <UserForm
        passwordInput={false}
        userTypeInput={false}
        readonly={!user.createdBySuperAdmin}
        onSubmit={handleSubmit}
        submitBtn={<Button type="submit">Save Changes</Button>}
        initialData={{
          ...user
        }}
      />
    </div>
  );
}

export default EditUserForm;
