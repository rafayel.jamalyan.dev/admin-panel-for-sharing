import Icon from "@/common/components/icon";
import ImagePicker from "@/common/components/image-picker";
import Image from "next/image";
import { useRef } from "react";
import { Noop, RefCallBack } from "react-hook-form";

interface Props {
  field: {
    onChange: (file: File | null) => void;
    onBlur: Noop;
    name: string;
    ref: RefCallBack;
  };
  readonly?: boolean;
  fileError?: string;
  setFileError: (error?: string) => void;
  defaultSrc?: string;
}

export default function UserPhotoPicker({ field, fileError, readonly, setFileError, defaultSrc }: Props) {
  const defaultSrcRef = useRef(defaultSrc);
  const handleImageChange = (file: File | null) => {
    field.onChange(file);
    if (fileError) setFileError(undefined);
  };

  return (
    <ImagePicker
      {...field}
      src={defaultSrcRef.current}
      readonly={readonly}
      crop
      cropAspectRatio={1 / 1}
      onChange={handleImageChange}
    >
      {(src, onClick, removeImage) => (
        <div className="flex items-center">
          <div className="overflow-hidden rounded-full">
            <Image
              src={src || "/images/user-placeholder.svg"}
              width="0"
              height="0"
              alt="user"
              style={{ width: 66, height: 66 }}
              unoptimized
            />
          </div>
          {!readonly && (
            <div className="flex flex-col">
              <div onClick={onClick} className="flex cursor-pointer items-center gap-1 px-4 py-1">
                <Icon name="camera-black" size={16} />
                <p className="text-base font-medium text-gray-800">{src ? "Upload new photo" : "Upload photo"}</p>
              </div>
              {src && (
                <div className="flex cursor-pointer items-center gap-1 px-4 py-1" onClick={removeImage}>
                  <Icon name="minus-outlined-red" size={16} />
                  <p className="text-base font-medium text-red-400">Remove</p>
                </div>
              )}
            </div>
          )}
        </div>
      )}
    </ImagePicker>
  );
}
