import React from "react";

type UserType = "trainer" | "customer";

interface Props {
  value: UserType;
  onChange: (value: UserType) => void;
}

function UserTypePicker({ value, onChange }: Props) {
  return (
    <div className="flex gap-4">
      <div
        className={`flex h-[43px] w-[132px] cursor-pointer items-center justify-center rounded-lg px-2.5 py-3 ${
          value === "trainer" ? "border-2 border-primary" : "border border-gray-50"
        }`}
        onClick={() => onChange("trainer")}
      >
        <p>Trainer</p>
      </div>
      <div
        className={`flex h-[43px] w-[132px] cursor-pointer items-center justify-center rounded-lg px-2.5 py-3 ${
          value === "customer" ? "border-2 border-primary" : "border border-gray-50"
        }`}
        onClick={() => onChange("customer")}
      >
        <p>Trainee</p>
      </div>
    </div>
  );
}

export default UserTypePicker;
