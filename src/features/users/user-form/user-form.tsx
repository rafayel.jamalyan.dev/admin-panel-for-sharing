"use client";

import UserPhotoPicker from "@/features/users/user-form/user-photo-picker";
import { zodResolver } from "@hookform/resolvers/zod";
import { useMemo, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import * as z from "zod";
import UserTypePicker from "./user-type-picker";
import Input from "@/common/components/input";
import Select from "@/common/components/select";
import PasswordInput from "@/common/components/password-input";
import { getFile } from "@/common/lib";
import { _USER_ALREADY_EXISTS_ } from "@/api/error-codes";
import { format } from "date-fns";

const validationSchema = (passwordInput: boolean, userTypeInput: boolean) =>
  z.object({
    name: z.string().min(2, "Name must be at least 2 characters").max(50, "Name must be at most 50 characters"),
    username: z.string().min(2, "Username must be at least 2 characters").max(50, "Username must be at most 50 characters"),
    email: z.string().email("Invalid email address"),
    gender: z.enum(["male", "female"]),
    dateOfBirth: z
      .string()
      .regex(/^\d{4}-\d{2}-\d{2}$/)
      .nullable(),
    photo: z.any(),
    ...(userTypeInput ? { role: z.enum(["trainer", "customer"]) } : { role: z.enum(["trainer", "customer"]).optional() }),
    ...(passwordInput
      ? {
          password: z.string().min(8, "Password must be at least 8 characters").max(50, "Password must be at most 50 characters")
        }
      : { password: z.string().optional() })
  });

export type UserFormData = z.infer<ReturnType<typeof validationSchema>>;

interface Props {
  initialData?: Partial<UserFormData>;
  passwordInput?: boolean;
  userTypeInput?: boolean;
  readonly?: boolean;
  onSubmit?: (data: UserFormData) => void;
  submitBtn?: React.ReactNode;
}

function UserForm({ initialData, passwordInput = true, userTypeInput = true, readonly, onSubmit, submitBtn }: Props) {
  const [fileError, setFileError] = useState<string>();
  const { control, handleSubmit } = useForm<UserFormData>({
    resolver: zodResolver(validationSchema(passwordInput, userTypeInput)),
    defaultValues: {
      name: initialData?.name || "",
      username: initialData?.username || "",
      email: initialData?.email || "",
      gender: initialData?.gender || "male",
      dateOfBirth: initialData?.dateOfBirth ? format(new Date(initialData!.dateOfBirth), "yyyy-MM-dd") : "",
      photo: undefined,
      ...(passwordInput ? { password: initialData?.password || "" } : {}),
      ...(userTypeInput ? { role: initialData?.role || "customer" } : {})
    }
  });
  const initialPhoto = initialData?.photo;
  const defaultPhotoSrc = useMemo(() => (initialPhoto ? getFile(initialPhoto) : initialPhoto), [initialPhoto]);

  async function submit(data: UserFormData) {
    if ("photo" in data && data.photo === undefined) delete data.photo;
    onSubmit?.(data);
  }

  return (
    <div>
      <form
        onSubmit={handleSubmit(data => {
          submit(data);
        })}
        autoComplete="off"
      >
        <div className="p-8">
          <div className="flex flex-col gap-8">
            <div>
              <Controller
                name="photo"
                control={control}
                render={({ field: { value: _dontNeedTheValueForTheFile, ...field } }) => {
                  return (
                    <UserPhotoPicker
                      defaultSrc={defaultPhotoSrc}
                      readonly={readonly}
                      field={field}
                      fileError={fileError}
                      setFileError={setFileError}
                    />
                  );
                }}
              />
            </div>
            <div>
              {userTypeInput && (
                <div className="border-b border-gray-50">
                  {!readonly && <p className="py-2 text-base font-medium text-gray-800">USER TYPE</p>}
                  <div className="py-4">
                    <Controller
                      name="role"
                      control={control}
                      render={({ field: { value, onChange } }) => <UserTypePicker value={value!} onChange={onChange} />}
                    />
                  </div>
                </div>
              )}
              <div className="border-b border-gray-50 py-4">
                {!readonly && <p className="py-2 text-base font-medium text-gray-800">USER REGISTRAITION DETAILS</p>}
                <div className="mt-4 grid grid-cols-2 gap-4">
                  <div>
                    <Controller
                      name="name"
                      control={control}
                      render={({ field }) => (
                        <Input {...field} readOnly={readonly} label="Name" placeholder="enter full name here" />
                      )}
                    />
                  </div>
                  <div>
                    <Controller
                      name="username"
                      control={control}
                      render={({ field }) => (
                        <Input
                          {...field}
                          readOnly={readonly}
                          autoComplete="off"
                          label="Username"
                          placeholder="enter preferred username here"
                        />
                      )}
                    />
                  </div>
                  <div>
                    <Controller
                      name="email"
                      control={control}
                      render={({ field }) => (
                        <Input {...field} readOnly={readonly} autoComplete="off" label="Email" placeholder="email@example.com" />
                      )}
                    />
                  </div>
                  {passwordInput && (
                    <div>
                      <Controller
                        name="password"
                        control={control}
                        render={({ field }) => (
                          <PasswordInput
                            {...field}
                            autoComplete="off"
                            readOnly={readonly}
                            copy
                            label="Initial Password"
                            placeholder="********"
                          />
                        )}
                      />
                    </div>
                  )}
                </div>
              </div>
              <div className="pt-4">
                {!readonly && <p className="py-2 text-base font-medium text-gray-800">PERSONAL DETAILS</p>}
                <div className="mt-4 grid grid-cols-2 gap-4">
                  <div>
                    <Controller
                      name="gender"
                      control={control}
                      render={({ field }) => (
                        <Select {...field} label="Gender" readOnly={readonly}>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </Select>
                      )}
                    />
                  </div>
                  <div>
                    <Controller
                      name="dateOfBirth"
                      control={control}
                      render={({ field: { value, ...field } }) => (
                        <Input {...field} value={value || ""} readOnly={readonly} label="Date of Birth" type="date" />
                      )}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {!readonly && submitBtn && (
          <div className="border-t border-gray-100 px-8 py-4">
            <div className="flex justify-end">{submitBtn}</div>
          </div>
        )}
      </form>
    </div>
  );
}

export default UserForm;
