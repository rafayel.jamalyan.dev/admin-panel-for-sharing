import { UserSubscriptionFilter } from "@/api/slices/users/users-slice";
import { HTMLAttributes } from "react";
interface UserSubscriptionChipProps extends HTMLAttributes<HTMLSpanElement> {
  status: UserSubscriptionFilter;
}

export default function UserSubscriptionChip({ status, className, ...attributes }: UserSubscriptionChipProps) {
  return (
    <span
      className={`rounded-[10px] px-2.5 py-0.5 text-small font-medium capitalize ${
        status === "free"
          ? "bg-purple-100 text-purple-300"
          : status === "premium"
          ? "bg-primary-100 text-primary-900"
          : "bg-orange-100 text-orange-300"
      } ${className ?? ""}`}
      {...attributes}
    >
      {status}
    </span>
  );
}
