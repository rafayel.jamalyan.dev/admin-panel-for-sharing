import { createColumnHelper } from "@tanstack/react-table";
import { UserForTable } from "./hooks";
import { format } from "date-fns";
import Icon from "@/common/components/icon";
import UserPhoto from "../user-photo";
import UserSubscriptionChip from "../user-subscription-chip";
import { Tooltip } from "react-tooltip";
import { blackTitle } from "../../../../tailwind.config";
import IconButton from "@/common/components/icon-button";
import { enqueueSnackbar } from "notistack";
import Pulse from "@/common/components/pulse";

const columnHelper = createColumnHelper<UserForTable>();

export const columnsForUsersTable = [
  columnHelper.accessor("number", {
    header: () => <HeaderItem label="#" width={42} />,
    cell: props => (
      <div className="flex justify-center">
        <p className="text-base font-medium text-black-title">{props.getValue()}</p>
      </div>
    )
  }),
  columnHelper.accessor("name", {
    header: () => <HeaderItem label="Name" width={220} className="text-start" />,
    cell: props => <NameCell {...props.row.original} />
  }),
  columnHelper.accessor("dateOfBirth", {
    header: () => <HeaderItem label="Birthday" width={117} />,
    cell: props => (
      <div className="flex justify-center">
        <p className="text-base font-normal text-gray-800">
          {props.getValue() ? format(new Date(props.getValue()!), "MM/dd/yyyy") : ""}
        </p>
      </div>
    )
  }),
  columnHelper.accessor("gender", {
    header: () => <HeaderItem label="Gender" width={117} />,
    cell: props => (
      <div className="flex justify-center">
        <p className="text-base font-normal capitalize text-gray-800">{props.getValue()}</p>
      </div>
    )
  }),
  columnHelper.accessor("type", {
    header: () => <HeaderItem label="Type" width={117} />,
    cell: props => (
      <div className="flex justify-center">
        <p className="text-base font-normal capitalize text-gray-800">
          {props.getValue() === "customer" ? "Trainee" : "Trainer"}
        </p>
      </div>
    )
  }),
  columnHelper.accessor("status", {
    header: () => <HeaderItem label="Status" width={117} />,
    cell: props => (
      <div className="flex items-center justify-center">
        <UserSubscriptionChip status={props.getValue()} data-tooltip-id={`status-${props.row.original.id}`} />
        {props.row.original.promocode && (
          <Tooltip
            id={`status-${props.row.original.id}`}
            style={{ background: blackTitle, padding: "8px 16px", borderRadius: "8px" }}
            clickable
          >
            <div className="flex flex-nowrap gap-2.5">
              <p className="text-small font-medium text-white">Generated code: {props.row.original.promocode}</p>
              <div className="-m-3">
                <IconButton
                  name="copy"
                  color="white"
                  size={16}
                  onClick={() => {
                    navigator.clipboard.writeText(props.row.original.promocode || "");
                    enqueueSnackbar("Copied to clipboard", { variant: "success", autoHideDuration: 2000 });
                  }}
                />
              </div>
            </div>
          </Tooltip>
        )}
      </div>
    )
  }),
  columnHelper.accessor("endDate", {
    header: () => <HeaderItem label="End Date" width={117} />,
    cell: props => (
      <div className="flex items-center justify-center">
        <p className="text-base font-normal capitalize text-gray-800">
          {props.getValue() ? format(new Date(props.getValue()!), "MM/dd/yyyy") : ""}
        </p>
      </div>
    )
  })
];

interface HeaderItemProps {
  label: string;
  width?: number;
  className?: string;
}

function HeaderItem({ label, width, className }: HeaderItemProps) {
  return (
    <p className={`text-base font-medium text-black-title ${className}`} style={{ width }}>
      {label}
    </p>
  );
}

function NameCell({ photo, isEmailVerified, name, email, id, promocode, hasRequestedForPromocode }: UserForTable) {
  const showPulse = hasRequestedForPromocode && !promocode;

  return (
    <div className="flex w-[220px] items-center">
      <div className="flex flex-nowrap items-center gap-2">
        <div className="relative">
          {showPulse && (
            <div className="absolute -left-[4px] top-0">
              <Pulse
                layers={[
                  { size: 4, className: "bg-accent-300" },
                  { size: 7, className: "bg-[#AFF1BC]" }
                ]}
              />
            </div>
          )}
          <div className="relative aspect-square w-[35px] overflow-hidden rounded-full">
            <UserPhoto photo={photo} />
          </div>
          {isEmailVerified && (
            <div className="absolute -right-[1px] -top-[1px]">
              <Icon name="verified-primary" size={16} data-tooltip-id={`user-verified-${id}`} />
              <Tooltip id={`user-verified-${id}`} style={{ background: blackTitle, padding: "8px 16px", borderRadius: "8px" }}>
                <p className="text-small font-medium text-white">Verified</p>
              </Tooltip>
            </div>
          )}
        </div>
        <div>
          <p className="max-w-[220px] overflow-hidden text-ellipsis text-base font-medium text-black-title">{name}</p>
          <p className="mt-1 max-w-[220px] overflow-hidden text-ellipsis text-small font-normal text-gray-800">{email}</p>
        </div>
      </div>
    </div>
  );
}
