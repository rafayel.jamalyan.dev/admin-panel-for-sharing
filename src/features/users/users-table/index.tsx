import React from "react";
import { getCoreRowModel, useReactTable } from "@tanstack/react-table";
import { useUsersContext } from "./hooks";
import TablePagination from "../../table/pagination";
import { columnsForUsersTable } from "./columns";
import UsersTableHeader from "./users-table-header";
import UsersTableHead from "./users-table-head";
import UsersTableBody from "./users-table-body";

const UsersTable = () => {
  const { users, total, page, setPage, perPage, setPerPage, reload } = useUsersContext();
  const table = useReactTable({
    data: users,
    columns: columnsForUsersTable,
    getCoreRowModel: getCoreRowModel()
  });

  const pageCount = Math.ceil(total / perPage);

  return (
    <div className="overflow-x-auto">
      <UsersTableHeader />
      <table className="border-separate border-spacing-0">
        <UsersTableHead table={table} />
        <UsersTableBody table={table} onDataChange={reload} />
      </table>
      <div className="px-4 py-3">
        <TablePagination
          page={page}
          setPage={setPage}
          perPage={perPage}
          setPerPage={setPerPage}
          total={total}
          pageCount={pageCount}
        />
      </div>
    </div>
  );
};

export default UsersTable;
