"use client";

import React, { useMemo } from "react";
import { useUsers, UserForTable, UsersContext } from "./hooks";
import UsersTable from ".";
import { getUserSubscriptionStatus } from "../utils";

function UsersTableWrapper() {
  const { users, total, ...data } = useUsers();
  const numberOffset = (data.page - 1) * data.perPage;

  const usersForTable = useMemo<UserForTable[] | null>(() => {
    if (!users) return null;
    return users.map((user, i) => {
      const status = getUserSubscriptionStatus(user);
      return {
        ...user,
        number: numberOffset + i + 1,
        status,
        endDate: (status === "premium" && user.subscription?.endsAt) || null
      };
    });
  }, [users, numberOffset]);

  if (!usersForTable) return null;

  return (
    <UsersContext.Provider value={{ users: usersForTable, total, ...data }}>
      <UsersTable />
    </UsersContext.Provider>
  );
}

export default UsersTableWrapper;
