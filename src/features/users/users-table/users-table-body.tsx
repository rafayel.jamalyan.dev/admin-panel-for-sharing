import React, { useState } from "react";
import { UserForTable } from "./hooks";
import { Table, flexRender } from "@tanstack/react-table";
import PopupMenu from "@/common/components/popup-menu";
import IconButton from "@/common/components/icon-button";
import { enqueueSnackbar } from "notistack";
import Api from "@/api";
import { startGlobalLoading, stopGlobalLoading } from "@/common/components/global-loading-provider";
import GenerateCodeModal from "../generate-code-modal";
import { getUserSubscriptionStatus } from "../utils";

interface Props {
  table: Table<UserForTable>;
  onDataChange?: () => void;
}

function UsersTableBody({ table, onDataChange }: Props) {
  const [userIdWhoHasRequestedForPromocode, setUserIdWhoHasRequestedForPromocode] = useState<number | null>(null);
  const isGenerateCodeModalOpen = userIdWhoHasRequestedForPromocode !== null;

  async function handleRemoveUser(user: UserForTable) {
    const shouldRemove = confirm(`Are you sure you want to remove ${user.name}?`);
    if (!shouldRemove) return;

    startGlobalLoading();
    const rsp = await Api.users.RemoveUser(user.id);
    stopGlobalLoading();

    if (rsp.meta.error) {
      return enqueueSnackbar("Could not remove user", { variant: "error", autoHideDuration: 2000 });
    }

    enqueueSnackbar(`${user.name} has been removed`, { variant: "success", autoHideDuration: 2000 });
    onDataChange?.();
  }

  return (
    <tbody>
      {table.getRowModel().rows.map(row => (
        <tr key={`row-${row.id}`} className="px-4 first:!pl-0 last:!pr-0 hover:bg-gray-50">
          {row.getVisibleCells().map((cell, i) => (
            <td key={`cell-${cell.id}`} className={`py-3 ${i === 0 ? "align-middle" : "align-top"}`}>
              {flexRender(cell.column.columnDef.cell, cell.getContext())}
            </td>
          ))}
          <td className="py-3 align-top">
            <div className="flex justify-center">
              <PopupMenu
                items={[
                  {
                    label: row.original.subscription?.status === "succeeded" ? "Manage Subscription" : "Upgrade Subscription",
                    href: `/users/${row.original.id}/upgrade-subscription`,
                    icon: "crown-primary"
                  },
                  {
                    label: "Generate Code",
                    onClick: () => setUserIdWhoHasRequestedForPromocode(row.original.id),
                    icon: "code-black",
                    disabled: getUserSubscriptionStatus(row.original) === "premium" || row.original.promocode !== null
                  },
                  {
                    label: "User Profile",
                    href: `/users/${row.original.id}/profile`,
                    icon: "user-outlined-black"
                  },
                  {
                    label: "Remove User",
                    onClick: () => handleRemoveUser(row.original),
                    icon: "minus-outlined-red",
                    labelClassName: "text-red-400"
                  }
                ]}
              >
                {handleAnchorElementClick => (
                  <IconButton
                    name="more-vertical"
                    className="p-0 active:shadow-primary100_4"
                    color="black"
                    buttonSize="xs"
                    size={24}
                    onClick={handleAnchorElementClick}
                  />
                )}
              </PopupMenu>
            </div>
          </td>
        </tr>
      ))}
      <GenerateCodeModal
        isOpen={isGenerateCodeModalOpen}
        onRequestClose={() => setUserIdWhoHasRequestedForPromocode(null)}
        userId={userIdWhoHasRequestedForPromocode}
        ariaHideApp={false}
      />
    </tbody>
  );
}

export default UsersTableBody;
