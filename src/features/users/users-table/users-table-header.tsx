import PopupMenu from "@/common/components/popup-menu";
import React from "react";
import { useUsersContext } from "./hooks";
import Checkbox from "@/common/components/checkbox";
import { UserSubscriptionFilter } from "@/api/slices/users/users-slice";
import IconButton from "@/common/components/icon-button";
import Icon from "@/common/components/icon";
import Input from "@/common/components/input";
import Pulse from "@/common/components/pulse";

function UsersTableHeader() {
  const { total, usersCountWhoHasRequestedForPromocode, query, setQuery, subscription, setSubscription } = useUsersContext();

  return (
    <div className="px-8 pb-2 pt-6">
      <div className="flex items-center justify-between">
        <div className="flex items-center gap-4">
          <PopupMenu content={<FilterContent subscription={subscription} onSubscriptionChange={setSubscription} />}>
            {handleAnchorElementClick => <IconButton name="filter" color="black" size={24} onClick={handleAnchorElementClick} />}
          </PopupMenu>
          <Input
            className="h-[33px] w-[322px]"
            placeholder={
              <div className="flex items-center gap-1">
                <Icon name="search-gray" size={16} />
                <p className="text-base font-medium text-gray-300">Search...</p>
              </div>
            }
            value={query}
            onChange={e => setQuery(e.target.value)}
          />
        </div>
        <div className="relative">
          {usersCountWhoHasRequestedForPromocode > 0 && (
            <div className="absolute -left-[7px] top-0">
              <Pulse
                layers={[
                  { size: 4, className: "bg-accent-300" },
                  { size: 7, className: "bg-[#AFF1BC]" }
                ]}
              />
            </div>
          )}
          <div className="flex items-center gap-0.5">
            <Icon name="user-black" size={16} />
            <p className="text-base font-medium text-black-title">{total} Users</p>
          </div>
        </div>
      </div>
    </div>
  );
}

interface FilterContentProps {
  subscription: UserSubscriptionFilter[];
  onSubscriptionChange: (value: UserSubscriptionFilter[]) => void;
}

function FilterContent({ subscription, onSubscriptionChange }: FilterContentProps) {
  function handleChange(type: UserSubscriptionFilter, checked: boolean) {
    const alreadyExists = subscription.includes(type);
    if (alreadyExists && !checked) {
      onSubscriptionChange(subscription.filter(sub => sub !== type));
    }
    if (!alreadyExists && checked) {
      onSubscriptionChange([...subscription, type]);
    }
  }
  return (
    <div className="rounded-[5px] bg-white p-4">
      <div className="flex flex-col gap-0.5">
        <FilterContentItem
          label="Free"
          checked={subscription.includes("free")}
          onChange={checked => handleChange("free", checked)}
        />
        <FilterContentItem
          label="Premium"
          checked={subscription.includes("premium")}
          onChange={checked => handleChange("premium", checked)}
        />
        <FilterContentItem
          label="Pending"
          checked={subscription.includes("pending")}
          onChange={checked => handleChange("pending", checked)}
        />
      </div>
    </div>
  );
}

interface FilterContentItemProps {
  label: string;
  checked: boolean;
  onChange: (checked: boolean) => void;
}

function FilterContentItem({ label, checked, onChange }: FilterContentItemProps) {
  return (
    <div className="p-2 hover:bg-primary-50">
      <Checkbox label={label} checked={checked} onChange={e => onChange(e.target.checked)} />
    </div>
  );
}

export default UsersTableHeader;
