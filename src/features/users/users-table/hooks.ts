import Api from "@/api";
import { User, UserSubscriptionFilter } from "@/api/slices/users/users-slice";
import { useDebouncedValue } from "@/common/hooks/debounce";
import React, { useCallback, useContext, useEffect, useState } from "react";

let pageCache = 1;
let perPageCache = 10;
let queryCache = "";

export function resetUsersTableCache() {
  pageCache = 1;
  perPageCache = 10;
  queryCache = "";
}

export function useUsers() {
  const [page, setPage] = useState(pageCache);
  pageCache = page;

  const [perPage, setPerPage] = useState(perPageCache);
  perPageCache = perPage;

  const [query, setQuery] = useState(queryCache);
  queryCache = query;

  const debouncedQuery = useDebouncedValue(query, 300);
  const [subscription, setSubscription] = useState<UserSubscriptionFilter[]>([]);
  const { data, loading, error, success, reload } = Api.useApi(
    () => Api.users.GetUsers(page, perPage, debouncedQuery, subscription),
    [page, perPage, debouncedQuery, subscription]
  );

  const usersRsp = (success && data?.items) || null;
  const usersCountWhoHasRequestedForPromocodeRsp = (success && data?.usersCountWhoHasRequestedForPromocode) || 0;

  const [users, setUsers] = useState<User[] | null>(null);
  const [usersCountWhoHasRequestedForPromocode, setUsersCountWhoHasRequestedForPromocode] = useState(0);

  // update users when usersRsp changes
  useEffect(() => {
    setUsers(usersRsp);
  }, [usersRsp]);

  // update usersCountWhoHasRequestedForPromocode when usersCountWhoHasRequestedForPromocodeRsp changes
  useEffect(() => {
    setUsersCountWhoHasRequestedForPromocode(usersCountWhoHasRequestedForPromocodeRsp);
  }, [usersCountWhoHasRequestedForPromocodeRsp]);

  const total = (success && data?.total) || 0;

  const changePerPage = useCallback((perPage: number) => {
    setPerPage(perPage);
    setPage(1);
  }, []);

  const changeQuery = useCallback((query: string) => {
    setQuery(query);
    setPage(1);
  }, []);

  const changeSubscription = useCallback((subscription: UserSubscriptionFilter[]) => {
    setSubscription(subscription);
    setPage(1);
  }, []);

  return {
    page,
    setPage,
    perPage,
    setPerPage: changePerPage,
    query,
    setQuery: changeQuery,
    subscription,
    setSubscription: changeSubscription,
    users,
    setUsers,
    total,
    usersCountWhoHasRequestedForPromocode,
    setUsersCountWhoHasRequestedForPromocode,
    loading,
    error,
    success,
    reload
  };
}

export type UserForTable = User & {
  number: number;
  status: UserSubscriptionFilter;
  endDate: string | null;
};

export const UsersContext = React.createContext<(Omit<ReturnType<typeof useUsers>, "users"> & { users: UserForTable[] }) | null>(
  null
);

export function useUsersContext() {
  const context = useContext(UsersContext);
  if (!context) throw new Error("useUsersContext must be used within a UsersProvider");
  return context;
}
