import { Table, flexRender } from "@tanstack/react-table";
import React from "react";
import { UserForTable } from "./hooks";

interface Props {
  table: Table<UserForTable>;
}

function UsersTableHead({ table }: Props) {
  return (
    <thead>
      {table.getHeaderGroups().map(headerGroup => (
        <tr key={`header-row-${headerGroup.id}`}>
          {headerGroup.headers.map(column => (
            <th colSpan={column.colSpan} key={`header-column-${column.id}`} className="px-4 py-5 first:!pl-0 last:!pr-0">
              {flexRender(column.column.columnDef.header, column.getContext())}
            </th>
          ))}
          {/* th for actions column */}
          <th>
            <div className="w-[117px]" />
          </th>
        </tr>
      ))}
    </thead>
  );
}

export default UsersTableHead;
