import Button from "@/common/components/button";
import Icon from "@/common/components/icon";
import { Props as ModalProps } from "react-modal";
import Modal from "@/common/components/modal";
import SelectionButton from "@/common/components/selection-button";
import { useState } from "react";
import { AlternatePaymentMethod } from "@/api/slices/users/users-slice.d";

interface Props extends ModalProps {
  onSelect?: (paymentMethod: AlternatePaymentMethod) => void;
}

export default function UserPaymentMethodPickerModal({ onSelect, ...modalProps }: Props) {
  return (
    <Modal {...modalProps}>
      <UserPaymentMethodPickerModalContent onSelect={onSelect} />
    </Modal>
  );
}

interface UserPaymentMethodPickerModalContentProps {
  onSelect?: (paymentMethod: AlternatePaymentMethod) => void;
}

export function UserPaymentMethodPickerModalContent({ onSelect }: UserPaymentMethodPickerModalContentProps) {
  const [paymentMethod, setPaymentMethod] = useState<AlternatePaymentMethod>(AlternatePaymentMethod.Cash);

  function handleSelect() {
    onSelect?.(paymentMethod);
  }

  return (
    <div className="rounded-[24px] bg-white">
      <div className="px-[120px] py-[48px]">
        <div className="flex flex-col items-center">
          <div className="flex justify-center">
            <Icon name="money-transfer" size={100} />
          </div>
          <div className="mt-6 flex flex-col items-center gap-2">
            <p className="text-h2 font-normal text-black-title">Select User Payment Method</p>
          </div>
          <div className="mt-4 flex flex-nowrap justify-center gap-4">
            <SelectionButton
              selected={paymentMethod === AlternatePaymentMethod.Cash}
              className="w-[132px]"
              onClick={() => {
                setPaymentMethod(AlternatePaymentMethod.Cash);
              }}
            >
              Cash
            </SelectionButton>
            <SelectionButton
              selected={paymentMethod === AlternatePaymentMethod.Coupons}
              className="w-[132px]"
              onClick={() => {
                setPaymentMethod(AlternatePaymentMethod.Coupons);
              }}
            >
              Coupons
            </SelectionButton>
          </div>
        </div>
      </div>
      <div className="border-t border-gray-300 p-[10px]">
        <div className="flex justify-center">
          <Button className="w-[260px]" variant="filled" onClick={handleSelect}>
            Select
          </Button>
        </div>
      </div>
    </div>
  );
}
