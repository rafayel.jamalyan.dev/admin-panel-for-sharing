import { getFile } from "@/common/lib";
import Image from "next/image";

interface Props {
  photo: string | null;
}

export default function UserPhoto({ photo }: Props) {
  return <Image alt="user-photo" src={photo ? getFile(photo) : "/images/user-placeholder.svg"} fill unoptimized />;
}
