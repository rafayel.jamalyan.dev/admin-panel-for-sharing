import { User } from "@/api/slices/users/users-slice";

export type UserSubscriptionStatus = "free" | "premium" | "pending";

export function getUserSubscriptionStatus(user: User): UserSubscriptionStatus {
  if (user.subscription?.status === "succeeded") return "premium";
  if (user.hasRequestedForPromocode) return "pending";
  return "free";
}
