#!/bin/bash

# variables $GL_STAGING_PORT, $CI_REGISTRY_IMAGE
# are defined by GitLab CI/CD

# pull the latest image from GitLab registry
docker pull $CI_REGISTRY_IMAGE

# if a docker container with name "admin" is running
# then stop it and remove it
if [ "$(docker ps -q -f name=admin)" ]; then
    docker stop admin
    docker rm admin
fi

# start a new container with name "admin"
docker run -d --name admin -p $GL_PORT:3000 $CI_REGISTRY_IMAGE
