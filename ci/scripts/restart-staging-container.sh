#!/bin/bash

# variables $GL_STAGING_PORT, $CI_REGISTRY_IMAGE, $CI_COMMIT_BRANCH
# are defined by GitLab CI/CD

echo "registry image is $CI_REGISTRY_IMAGE"
echo "branch is $CI_COMMIT_BRANCH"
echo "staging port is $GL_STAGING_PORT"

# pull the latest image from GitLab registry
docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH

# if a docker container with name "admin-staging" is running
# then stop it and remove it
if [ "$(docker ps -q -f name=admin-staging)" ]; then
    docker stop admin-staging
    docker rm admin-staging
fi

# start a new container with name "admin-staging"
docker run -d --name admin-staging -p $GL_STAGING_PORT:3000 $CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH
